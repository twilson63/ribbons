const test = require('tape')

const ribbons = require('../')
const db = ribbons(':memory:')
const dogs = db('dogs', ['type'])
const dogs2 = db('dogs2', ['type'])


test('list all dogs', async t => {
  await setup(dogs)
  const allDogs = await dogs.all()
  t.equal(allDogs.length, 4)
  t.end()
})

test('list 2 dogs', async t => {
  await setup(dogs2)
  const twoDogs = await dogs2.all({limit: 2})
  t.equal(twoDogs.length, 2)
  t.end()
})

test('list all ids that start with dog-r', async t => {
  const rdogs = await dogs.all({start: 'dog-r', end: 'dog-rz'})
  t.equal(rdogs.length, 1)
  t.end()
})

test('list dogs by key', async t => {
  const threedogs = await dogs.all({keys: ['dog-wilbur', 'dog-gretal', 'dog-carie']})
  t.equal(threedogs.length, 3)
  t.end()
})


function setup(dogs) {
  return Promise.all([
    dogs.post({ type: 'dog', _id: 'dog-wilbur', name: 'wilbur', breed: 'jack russell terrier'}),
    dogs.post({ type: 'dog', _id: 'dog-carie', name: 'carie', breed: 'jack russell terrier'}),
    dogs.post({ type: 'dog', _id: 'dog-gretal', name: 'gretal', breed: 'jack russell terrier'}),
    dogs.post({ type: 'dog', _id: 'dog-ribbons', name: 'ribbons', breed: 'jack russell terrier'})
  ])
}




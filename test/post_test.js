const test = require('tape')
const ribbons = require('../')
const db = ribbons(':memory:')

const cogs = db('cogs', ['type'])

test('post document success', t => {
  cogs.post({
    _id: 'foo',
    name: 'cog1',
    type: 'cog'
  })
    .then(res => {
      console.log(res)
      t.ok(res.ok)
      t.ok(res.id)
      t.ok(res.rev)
      t.end()
    })
    .catch(e => {
      t.ok(false, e.message)
    })
})

  /*
test('post document fail no keys', t => {
  t.plan(1)
  cogs.post({
    name: 'beep'
  })
    .then(r => {
      console.log(r)
      
      cogs.get(r.id)
        .then(d => {
          console.log(d)
          t.ok(false, 'this should fail')
      })
          
    })
    .catch(e => {
      t.ok(true, e.message)
    })
})
*/
test('post document fail', t => {
  cogs.post('bad food')
    .then(r => {
      console.log(r)
      t.end()
    })
    .catch(e => {
      t.ok(true, e.message)
      t.end()
    })
})


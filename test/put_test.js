const test = require('tape')
const ribbons = require('../')
const db = ribbons(':memory:')

const people = db('people', ['type'])

test('update doc', async t => {
  try {
    const res = await people.post({name: 'Trey', type: 'person'})
    const doc = await people.get(res.id)
    doc.age = 11
    const res2 = await people.put(doc)
    t.ok(res2.ok)
    const collection = await people.all()
    t.equal(collection.length, 1)
    t.end()
  } catch (e) {
    console.log(e.message)
    t.end()
  }
})

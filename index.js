/**
 * ribbons
 *
 * A nosql database model that uses sqlite3 as its 
 * persistence engine. This module allows you to embed
 * a db in your application without having to install
 * any external resources.
 *
 */
const sqlite3 = require('sqlite3')
const cuid = require('cuid')
const crypto = require('crypto')
const { map, compose, join, prop, head, split, omit, inc, values, keys,
 not, is, propOr } = require('ramda')

const parseJSON = JSON.parse.bind(JSON)
const sha256 = s => crypto.createHash('sha256').update(s).digest('hex')

/**
 * Getting Started
 *
 * ``` js
 * const ribbons = require('ribbons')
 * const db = ribbons('app.db')
 * const collection = db('widgets', ['type'])
 * collection.post({ type: 'widget', name: 'foo'})
 *   .then(res => console.log(res))
 * ```
 *
 * ribbons
 *
 * @param {string} database - name for database - ':memory:'
 * @return {function} - createCollection function
 */
module.exports = (database) => {
  
  const db = new sqlite3.Database(database) 
  /**
   * createCollection
   *
   * @param {string} table - name of collection
   * @param {array} keys - secondary indexes
   *
   * @return {object} - collection methods
   */
  return function (table, keys) {
    db.serialize(function() {
      db.run(createTable(table, keys), err => {
        if (err) { console.log(err) }
      })
      const doIndex = createIndex(table)
      keys.forEach(k => db.run(doIndex(k), noop))
    })
    return {
      all,
      post,
      put,
      get,
      remove,
      query,
      //bulkDocs
      //info
      close
    }

    /**
     * find documents based on criteria
     *
     * When using the query function, you can only 
     * search using the secondary indexes or keys, 
     * that are defined on your document.
     *
     * @param {object} whereValues - key value criteria
     * @return {Promise} - returns a promise that resolves
     * to an array of results
     */
    function query(whereValues={}) {
      return new Promise(function (resolve, reject) {
        let whereClause = Object.keys(whereValues).map(k => db.escapeId(k) + ' = ?').join(' AND ')
        let query = `SELECT document FROM ${table} WHERE ${whereClause}`
        
        db.all(query, values(whereValues), (err, results) => {
          if (err) { return reject(err) }
          resolve(map(compose(parseJSON, prop('document')), results))
        }) 
      })
    }

    /**
     * get all documents for the collection
     *
     * @param {object} options - filter options for retrieving alldocs
     *
     * @return {Promise} - resolves to array of results
     *
     */
    function all(options={limit: 20}) {
      return new Promise(function (resolve, reject) {
        function argufy(arr) {
          return compose(
            join(','),
            map(v => `'${v}'`)
          )(arr)
        }
        // get by keys
        // get by range - start, end
        // get by limit 
        //
        let query = `SELECT document FROM ${table}`
        if (options.keys) {
          query += ` WHERE _id in (${argufy(options.keys)})`
        } 
        if (options.start && options.end) {
          query += ` WHERE _id >= '${options.start}' AND _id < '${options.end}'`
        }

        query += ` LIMIT ${options.limit || 20}`
        db.all(query, (err, results) => {
          if (err) { return reject(err) }
          resolve(map(compose(parseJSON, prop('document')), results))
        })
      })

    }

    /**
     * remove a document
     *
     * @param {string} id - id of document to remove
     * @param {string} rev - version of doc to remove
     *
     * @return {Promise}
     *
     */
    function remove(id, rev) {
      return new Promise(function(resolve, reject) {
        db.get(`SELECT _rev FROM ${table} WHERE _id = ?`, [id], (err, result) => {
          if (err) { return reject(err) }
          console.log(result)
          if (!result) { return resolve({error: 'document not found'}) }
          if (result._rev !== rev) { return resolve({error: 'rev does not match'}) }
          db.run(`DELETE from ${table} WHERE _id = ? AND _rev = ?`, [id, rev], (err) => {
            if (err) { return reject(err) }
            resolve({ ok: true, id: id, rev: rev})
          })
        })
      })
    }

    /**
     * set or update document 
     *
     * @param {object} doc - document to update
     *
     * @return {Promise} - result of transaction
     *
     */
    function put(doc) {
      return new Promise(function(resolve,reject) {
        db.get(`SELECT _rev FROM ${table} WHERE _id = ?`,
          [doc._id], (err, row) => {
            if (err) { return reject(err) }
            // if no document and id specified add doc
            // if document but rev does not match reject
            if (!row) { return resolve(post(doc))  }
            const rev = row._rev
            if (rev !== doc._rev) { return resolve({error: 'document conflict'}) }
            // if document and rev match replace
            doc._rev = buildRev(doc)
            const values = {
              _id: doc._id,
              _rev: doc._rev,
              document: JSON.stringify(doc)
            }
            keys.forEach(k => values[k] = doc[k])
            
            const stmt = `
            UPDATE ${table} 
            SET 
              _rev = '${doc._rev}', 
              document = '${JSON.stringify(doc)}', 
              ${keys.map(k => `${k} = '${doc[k]}'`).join(', ')}, 
              _updated = '${new Date().toISOString()}', 
              _timestamp = ${Date.now()}
             
            WHERE _id = '${doc._id}'`
            if (process.env.DEBUG) { console.log(stmt) }
            db.run(stmt, (err, result) => {
              if (err) { return reject(err) }
              resolve({
                ok: true,
                id: doc._id,
                rev: doc._rev
              })
            })

          })

      })
    }

    /**
     * get document by id
     *
     * @param {string} id - unique identifier
     *
     * @return {Promise} - resolves to document
     */
    function get(id) {
      return new Promise(function (resolve, reject) {
        db.get(`select document from ${table} where _id = ?`, [id], (err, row) => {
          if (err) { return reject(err) }
          if (!row) { return resolve({error: 'not_found'}) }
          resolve(parseJSON(row.document))
        })
      })
    }

    /**
     * creates document
     *
     * @param {object} doc - document to store in collection/table
     * @return {Promise} - resolves result object 
     *
     */
    function post(doc) {
      return new Promise(function(resolve, reject) {
        if (not(is(Object, doc))) {
          return reject('doc must be an object')
        }
        const hash = sha256(JSON.stringify(doc))
        doc._id = doc._id || cuid()
        doc._rev = '1-' + hash
        
        const stmt = `INSERT INTO ${table} 
(_id, _rev, document, ${keys.join(', ')}, _created, _updated, _timestamp) 
VALUES ('${doc._id}', '${doc._rev}', '${JSON.stringify(doc)}' , 
${keys.map(k => "'" + propOr('', k, doc) + "'").join(', ')},
'${new Date().toISOString()}',
'${new Date().toISOString()}',
${Date.now()}
)` 
        if (process.env.DEBUG) { console.log(stmt) }
        db.run(stmt, (err) => {
          if (err) { return reject(err) }
          resolve({
            ok: true,
            id: doc._id,
            rev: doc._rev
          })
        })
      })
    }
    
    /**
     * close database
     *
     * @param {function} cb - callback function
     */
    function close(cb=noop) {
      db.close()
      cb()
    }
    
    /**
     * build create table command
     *
     * @param {string} table - name of table
     * @param {array} keys - secondary index names
     *
     * @return {string} - CREATE TABLE SQL Command
     */
    function createTable(table, keys) {
      return `
 CREATE TABLE IF NOT EXISTS ${table} (
   _id TEXT NOT NULL,
   _rev TEXT NOT NULL,
   document TEXT NOT NULL,
   ${keys.map(k => `${k} TEXT`).join(', ')},
   _created TEXT NOT NULL,
   _updated TEXT NOT NULL,
   _timestamp REAL NOT NULL,
   primary key(_id)
 )
 `
    }


    /**
     * build secondary index command
     *
     * To optimize searching for specific documents using 
     * secondary indexes, create an index for each secondary
     * index key.
     *
     * @param {string} table - name of table
     *
     * @return {function} - return a function to accept a key
     *
     */
    function createIndex(table) {

      /**
       * setKey
       *
       * defines the key for the secondary index
       *
       * @param {string} key - key name
       *
       * @return {string}
       *
       */
      return function (key) {
        return `CREATE INDEX idx_${key} ON ${table} (${key})`
      }
    }
    
    /**
     * create document revision value that will consist
     * of revision count and sha256
     */
    function buildRev(doc) {
      const sha = sha256(JSON.stringify(omit(['_id','_rev'], doc)))
      const version = compose(
        inc,
        head,
        split('-')
      )(doc._rev)
      return `${version}-${sha}`
    }

    /**
     * function that returns null
     */
    function noop() {
      return null
    }
  }

}
